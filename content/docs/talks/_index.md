---
weight: 1
type: docs
bookToc: false
---

# Talks and Media

A collection of news reports, presentations and blog article about Scuttlebutt.

## Video
<section id="proof-we-legit" class="bulletin">
  <div class="bulletin--grid">
    <figure class="bulletin--item">
      <a href="https://letstalkbitcoin.com/blog/post/epicenter-dominic-tarr-secure-scuttlebutt-the-localized-but-distributed-social-network">
      <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/27Iv8Jf4vZ0?&modestbranding=1" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; border:0;" allowfullscreen></iframe>
      </div>
        <div class="bulletin--caption">
          <p>The Localized but Distributed Social Network</p>
          <p>2019</p>
        </div>
      </a>
      <p>We’re joined by Dominic Tarr, a sailor, and the Founder of Secure Scuttlebutt. This curiously named project has a fascinating approach to creating a truly distributed social network.</p>
      <!-- <img id="emoji" src="/images/hermie-emojis/small-hermies-star.gif"> -->
    </figure>
    <figure class="bulletin--item">
      <a href="https://youtu.be/JSWWkzsHhjk?&modestbranding=1">
        <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/JSWWkzsHhjk" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; border:0;" allowfullscreen></iframe>
        </div>
        <div class="bulletin--caption">
          <p>Scuttlebutt and a Decentralized Future</p>
          <p>2018</p>
        </div>
      </a>
      <p>Scuttlebutt can be transformative for society, decentralizing and enabling local community development. Perfect for environments which require offline workability. </p>
    </figure>
    <figure class="bulletin--item">
      <a href="https://www.youtube.com/watch?v=8GE5C9-RUpg?&modestbranding=1">
        <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/8GE5C9-RUpg" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; border:0;" allowfullscreen></iframe>
        </div>
        <div class="bulletin--caption">
          <p>Reinvent the Social Web <br> André Staltz @ FullStackFest</p>
          <p>2018</p>
        </div>
      </a>
      <p>The times we live in have made it necessary to disrupt mainstream social networks. In this talk, Andre will present Scuttlebutt, from the Node.js hacker community. With fully offline browsing, new types of interactions, cryptographic authenticity, and data ownership, several opportunities for innovation are unlocked.</p>

    </figure>
    <figure class="bulletin--item">
      <a href="https://mixitconf.org/2019/the-internet-in-2030" target="_blank">
      <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;">
      <img src="/images/talks/andrestaltz-internet-in-2030.jpg" alt="video thumbnail">
      </div>
        <div class="bulletin--caption">
          <p>The internet in 2030</p>
          <p>André Staltz @ Mixit 19</p>
        </div>
      </a>
      <p>
      There are even more difficulties approaching: climate crisis, crowd social engineering, and the next impending economic crisis, all of which are interconnected. One promising approach can benefit the environment, the economy, humanity, and the internet: décroissance.
      </p>

    </figure>
    <figure class="bulletin--item">
            <a href="https://vimeo.com/283993534" target="_blank">
              <img src="/images/talks/dweb2018-dinosaur.jpg" alt="video thumbnail">
              <div class="bulletin--caption">
                <p>A Tour of the Scuttlebutt ecosystem</p>
                <p>DWeb Summit 2018</p>
              </div>
            </a>
            <p>Mikey Williams, aka dinosaur, is sort of the
            Scuttlebutt librarian, and gives us a quick tour.</p>
    </figure>
    <figure class="bulletin--item">
      <a href="https://www.youtube.com/watch?v=8GE5C9-RUpg" target="_blank">
        <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;">
        <img src="/images/talks/Anders (arj) @ Techfestival 2018.jpg" alt="video thumbnail">
        </div>
        <div class="bulletin--caption">
          <p>Anders (arj) @ Techfestival 2018</p>
        </div>
      </a>
      A talk given at the Algorithmic Sovereignty summit during Techfestival in Copenhagen. The talk touches on how Scuttlebutt can be used to regain some sovereignty over how data is shared and bring transparancy to the algorithms that runs on the data.
    </figure>
    <figure class="bulletin--item">
      <a href="https://decentralizedweb.net/videos/tech-talk-what-is-the-meaning-of-decentralization/">
      <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;">
      <img src="/images/talks/dweb-what-is-the-meaning-of-decentralization.png" alt="video thumbnail">
      </div>
        <div class="bulletin--caption">
          <p>What is the meaning of "decentralization"?</p>
          <p>Dweb Summit August 1 2018</p>
        </div>
      </a>
      <p>
      Taken literally, "decentralization" just means "not centralized",
      but what are the different ways something can be not centralized?
      Are there different types or flavors of decentralization that can be meaningfully distinguished?
      </p>

    </figure>
    <figure class="bulletin--item">
            <a href="https://youtu.be/UjfWAbGfPh0" target="_blank">
              <img src="/images/talks/andrestaltz-tedx-its-time-to-build-our-own-internet.jpg" alt="video thumbnail">
              <div class="bulletin--caption">
                <p>It's time to build our own Internet</p>
                <p>TEDxGeneva 2018</p>
              </div>
            </a>
            Smartphones are not to be blamed. The infrastructure of the Internet, often hidden from our sight, provided the technological foundational for the problems we are seeing today. It is possible to build an alternative Internet, that make a new foundation for freedom in the digital era.
    </figure>
    <figure class="bulletin--item">
      <a href="https://youtu.be/CcDMWrH6VHU" target="_blank">
        <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;">
        <img src="/images/talks/andrestaltz-braziljs-o-fim-da-internet.jpg" alt="video thumbnail">
        </div>
        <div class="bulletin--caption">
          <p>O fim da Internet</p>
          <p>BrazilJS Conf 2017</p>
        </div>
      </a>
      Nenhuma tecnologia desaparece da noite pro dia. Assim como o telefone fixo e o Flash, tecnologias sofrem uma gradual redução de uso que pode durar anos ou décadas até desaparecerem completamente. Os primeiros sinais do fim da internet original, aberta e decentralizada, já estão surgindo. Nesta palestra você vai descobrir quais são esses sinais, o que vai acontecer nos próximos anos, e como uma alternativa à internet está sendo criada, usando apenas JavaScript.
    </figure>
</section>

Find more videos [here](https://www.scuttlebutt.nz/talks)!
