---
weight: 5
---

# Research

Academic Research, Publications, and References

## Technical
<span class="sub-font">Technical papers and extensive guides</span>

- <a href="https://ssbc.github.io/scuttlebutt-protocol-guide/" class="research-title">Scuttlebutt Protocol Guide</a>
- <a href="https://github.com/dominictarr/scalable-secure-scuttlebutt/blob/master/paper.md" class="research-title">Scalable Secure Scuttlebutt</a>
- <a href="https://dominictarr.github.io/secret-handshake-paper/shs.pdf" class="research-title">Secret Handshake</a>
- <a href="https://github.com/dominictarr/epidemic-broadcast-trees" class="research-title">Epidemic Broadcast Trees  </a>


## Social
<span class="sub-font">Ethnographic and contextual studies of the Scuttlbutt ecosystem and distributed computing</span>

- <a href="http://ericklavoie.com/pre-print/pvc.pdf" class="research-title">Personal Volunteer Computing  </a>
- The Scuttleverse: Alternative Social Media for Decentralised Sociotechnical Communities?
