---
title: Docs
type: docs
bookToc: false
---

# Docs

## Scuttlebutt Documentation

<h1 align="center">
<img
    alt="Hermies the hermit crab"
    src="/images/ssbc.png"
    width="256"
    height="256"
  />
</h1>

<h2 align="center">
  a decent(ralised) secure gossip platform
</h2>

<p align="center">
 scuttlebutt - sea-slang for gossip - a scuttlebutt is a watercooler on a ship
</p>

&nbsp;

## Where to start

If you are looking to just jump in follow the [get started]({{< relref "/get-started" >}}) guide.



You can also learn more about the principles, technicals, or the kind of people behind Scuttlebutt.

New to scuttlebutt? Check out [Introduction](./introduction) and the Scuttlebutt Love Story.

## Contributor Code of Conduct

Please note that this project is released with a [Contributor Code of Conduct](/docs/code-of-conduct/). By participating in this project you agree to abide by its terms.





&nbsp;
